var use_random_sampling = true;
var use_cluster_sampling = false;
var use_pca = true;
var use_mds = false;
var use_isomap = false;

var output = {}

var k_value = 3;
parseJsonData();

$(".samples > .btn").click(function(){
    $(this).addClass("active").siblings().removeClass("active");
});

$(document).ready(function() {
      $("a.dropdown-toggle").click(function(e) {
          $("a.dropdown-toggle").dropdown("toggle");
          return false;
      });
      $("ul.dropdown-menu a").click(function(e) {
          $("a.dropdown-toggle").dropdown("toggle");
          k_value = e.currentTarget.innerText;
          return false;
      });
})

function parseJsonData() {
    //Parsing json data
    d3.json("data/white.json", function(error, data) {
        output = data;
    });
}

function set_random_sampling() {
    use_random_sampling = true;
    use_cluster_sampling = false;
}

function set_adaptive_sample() {
    use_random_sampling = false;
    use_cluster_sampling = false;
}

function set_cluster_sample() {
    use_random_sampling = false;
    use_cluster_sampling = true;
}

function pca() {
    if (use_random_sampling) {
        generate_scatterplot(output['pca_random'])
    }
    // if (use_adaptive_sampling) {
    //     generate_scatterplot(output['pca_adaptive'])
    // }
    if (use_cluster_sampling) {
        generate_scatterplot(output['pca_cluster'+k_value])
    }
}

function mds(type) {
    var suffix = '_euclidean';
    if(type == 2) {
        suffix = '_cosine';
    } else if(type == 3) {
        suffix = '_correlation';
    }
    if (use_random_sampling) {
        generate_scatterplot(output['mds_random'+suffix])
    }
    // if (use_adaptive_sampling) {
    //     generate_scatterplot(output['mds_adaptive'])
    // }
    if (use_cluster_sampling) {
        generate_scatterplot(output['mds_cluster'+suffix+k_value])
    }
}

function isomap() {
    if (use_random_sampling) {
        generate_scatterplot(output['isomap_random'])
    }
    // if (use_adaptive_sampling) {
    //     generate_scatterplot(output['isomap_adaptive'])
    // }
    if (use_cluster_sampling) {
        generate_scatterplot(output['isomap_cluster'+k_value])
    }
}

function text_viz() {
    generate_scatterplot(output['text_pts'])
}
function generate_scatterplot(data) {
    var margin = {
            top: 20,
            right: 50,
            bottom: 30,
            left: 40
        },
        width = 900 - margin.left - margin.right,
        height = 500 - margin.top - margin.bottom;
    // setup x
    var xValue = function(d) {
            return d.x;
        }, // data -> value
        xScale = d3.scale.linear().range([0, width]), // value -> display
        xMap = function(d) {
            return xScale(xValue(d));
        }, // data -> display
        xAxis = d3.svg.axis().scale(xScale).orient("bottom");
    var xValue_min = d3.min(data, xValue);
    var xValue_max = d3.max(data, xValue);

    // if(xValue_min < 0 && xValue_max < 1) {
    //     xValue = function(d) {
    //         var p = d.x - xValue_min;
    //         console.log(Math.log(p) - Math.log(1-p));
    //         return (Math.log(p) - Math.log(1-p));
    //     };
    // }
    // setup y
    var yValue = function(d) {
            return d.y;
        }, // data -> value
        yScale = d3.scale.linear().range([height, 0]), // value -> display
        yMap = function(d) {
            return yScale(yValue(d));
        }, // data -> display
        yAxis = d3.svg.axis().scale(yScale).orient("left");

    var yValue_min = d3.min(data, yValue);
    var yValue_max = d3.max(data, yValue);
    // if(yValue_min < 0 && yValue_max < 1) {
    //     yValue = function(d) {
    //         var p = d.y - yValue_min;
    //         console.log(Math.log(p) - Math.log(1-p));
    //         return (Math.log(p) - Math.log(1-p));
    //     };
    // }

    // setup fill color
    var cValue = function(d) {
            // console.log(d.c_type);
            return d.c_type;
        },
        color = d3.scale.category10();

    d3.select("svg").remove();

    var svg = d3.select("#chart").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    xScale.domain([d3.min(data, xValue), d3.max(data, xValue)]);
    yScale.domain([d3.min(data, yValue), d3.max(data, yValue)]);
    //transform data using log(p)/ (1-p)
    // console.log(d3.min(data, xValue));
    // console.log(d3.max(data, xValue));
    // x-axis
    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)
        .append("text")
        .attr("class", "label")
        .attr("x", width)
        .attr("y", -6)
        .style("text-anchor", "end")
        .text("x");

    // y-axis
    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
        .append("text")
        .attr("class", "label")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text("y");

    // draw dots
    svg.selectAll(".dot")
        .data(data)
        .enter().append("circle")
        .attr("class", "dot")
        .attr("r", 3)
        .attr("cx", xMap)
        .attr("cy", yMap)
        .style("fill", function(d) {
            return color(cValue(d));
        })
        .on("mouseover", function(d) {
            tooltip.transition()
                .duration(200)
                .style("opacity", .9);
            tooltip.html(d["c_type"] + "<br/> (" + xValue(d) + ", " + yValue(d) + ")")
                .style("left", (d3.event.pageX + 5) + "px")
                .style("top", (d3.event.pageY - 28) + "px");
        })
        .on("mouseout", function(d) {
            tooltip.transition()
                .duration(500)
                .style("opacity", 0);
        });
    // add the tooltip area to the webpage
    var tooltip = d3.select("body").append("div")
        .attr("class", "tooltip")
        .style("opacity", 0);

    // draw legend
    var legend = svg.selectAll(".legend")
        .data(color.domain())
        .enter().append("g")
        .attr("class", "legend")
        .attr("transform", function(d, i) {
            return "translate(0," + i * 20 + ")";
        });

    // draw legend colored rectangles
    legend.append("rect")
        .attr("x", width - 30)
        .attr("width", 18)
        .attr("height", 18)
        .style("fill", color);

    // draw legend text
    legend.append("text")
        .attr("x", width - 36)
        .attr("y", 9)
        .attr("dy", ".35em")
        .style("text-anchor", "end")
        .text(function(d) {
            return d;
        })
}

function show_scree_plot() {
    var margin = {
            top: 40,
            right: 100,
            bottom: 30,
            left: 150
        },
        width = 960 - margin.left - margin.right,
        height = 500 - margin.top - margin.bottom;
    d3.json("data/support_white.json", function(error, data) {
        // setup x
        data = data["kmeans_inertia"];
        var xValue = function(d) {
            return d.x;
        };
        // data -> value
        var xScale = d3.scale.ordinal()
            .domain(d3.range(1,21))
            .rangeRoundPoints([0, width]);

        // data -> display
        var xAxis = d3.svg.axis().scale(xScale).orient("bottom");

        var yValue = function(d) {
            return +d.y;
        };
        // data -> value
        var yScale = d3.scale.linear()
            .domain([d3.min(data, function(d) {
                        return yValue(d);
                    }), d3.max(data, function(d) {
                        return yValue(d);
                    })])
                    .range([height, 0]);

        var formatyAxis = d3.format("e");
        // data -> display
        var yAxis = d3.svg.axis().scale(yScale)
            .tickFormat(formatyAxis)
            .orient("left");

        var line = d3.svg.line()
            .x(function(d) {
                // console.log("x", xScale(xValue(d)));
                return xScale(xValue(d));
            })
            .y(function(d) {
                // console.log("y", yScale(yValue(d)));
                return yScale(yValue(d));
            });

        d3.select("svg").remove();

        var svg = d3.select("#chart").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis);

        svg.append("g")
            .attr("class", "y axis")
            .call(yAxis)
            .append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", -30)
            .attr("x", height)
            .attr("dy", ".71em")
            .style("text-anchor", "end")
            .text("Intertia");

        svg.append("path")
            .datum(data)
            .attr("class", "line")
            .attr("d", line);
    });
}