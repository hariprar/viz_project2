import string
import os
import json
import pandas as pd
import numpy as np
import nltk

from time import time
from random import sample

from sklearn import cross_validation
from sklearn import metrics

from sklearn.cluster import KMeans

from sklearn.metrics.pairwise import pairwise_distances

from sklearn.decomposition import PCA
from sklearn.decomposition import TruncatedSVD

from sklearn.manifold import MDS
from sklearn.manifold import Isomap

from sklearn.feature_extraction.text import TfidfVectorizer

from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import Normalizer
from collections import Counter

from nltk.stem.porter import PorterStemmer

from flask import Flask
app = Flask(__name__, static_url_path='/static')

# FILE_PATH = "data/covtype.data"
FILE_PATH = "data/winequality-white.csv"
READ_RESULTS_PATH = "data/white.json"
WRITE_RESULTS_PATH = "data/white.json"
DATA_SUPPORT_PATH = "data/support_white.json"
TEXT_FILE_PATH = "./data/text_data"

# @app.route('/')
# def index():
#     return app.send_static_file('./index.html')

def plot_data():
    with open(READ_RESULTS_PATH) as data_file:
        data = json.load(data_file)
    # for k in data:
    #     print k
def get_random_sample(df, rand_percent, indices=[]):
    # print "get_random_sample"
    # print df
    # print len(indices)
    sample_index = []
    if len(indices) == 0:
        result = cross_validation.ShuffleSplit(df.ix[:,0].count(), n_iter=1,
            train_size=rand_percent, random_state=0)
        for sample_index, ignore_index in result:
            # print("%s %s" % (sample_index, ignore_index))
            # print "Num rows in random sample =",len(sample_index)
            pass
    else:
        # print 'df size =', df.ix[:,0].count()
        num_rows = int(df.ix[:,0].count() * rand_percent)
        sample_index =  np.array(sample(indices, num_rows))
        # print 'sample size =', len(sample_index)
    # print sample_index
    result = df.ix[sample_index]
    # print result
    return result

def get_adaptive_sample(df, percent):
    bin_size = {}
    result = None
    c = Counter(df['c_type'])
    print "counter of c_type", c
    for i in c:
        q = 'c_type == '+str(i)
        tmp = df.query(q)
        row_indices = df.query(q).index.tolist()
        bin_df = get_random_sample(tmp, percent, row_indices)
        if result is None:
            result = bin_df
        else:
            result = result.append(bin_df)
        bin_size[i] = len(bin_df)
    return result, bin_size

def get_cluster_sample(df, percent, k=4):
    row_indices_dict = {}
    bin_size = {}
    result = None
    kmeans = perform_kmeans(df, k)
    c = Counter(kmeans.labels_)
    print c
    # Calculating number of samples to collect in each label
    for k in c:
        row_indices_dict[k] = []

    for i in xrange(len(kmeans.labels_)):
        row_indices_dict[kmeans.labels_[i]].append(i)

    for k in row_indices_dict:
        # print "row_indices_dict[%d]= %d" % (k, len(row_indices_dict[k]))
        bin_df = get_random_sample(df.ix[row_indices_dict[k]], percent, row_indices_dict[k])
        if result is None:
            result = bin_df
        else:
            result = result.append(bin_df)
        # print "bin_size[%d] = %d" % (k, len(bin_df))
        bin_size[k] = len(bin_df)
    return result, bin_size, row_indices_dict

#explained_variance_ratio_
# [ 6.62971993e-01   3.06602249e-01   1.76822034e-02   8.72530362e-03
#   3.17716422e-03   4.63273439e-04   2.47216109e-04   1.10643496e-04
#   1.09804362e-05   7.81159688e-06   5.53523752e-07   4.49600909e-07
#   1.58242493e-07 ]
def perform_pca(data, labels_dict=None):
    pca = PCA(n_components=2)
    pca.fit(data)
    # print pca.get_covariance()
    # print pca.explained_variance_ratio_
    # ctype = []
    # for c in data['c_type']:
    #     ctype.append(c)
    # print ctype, len(ctype)
    transformed_data = pca.transform(data)
    # print transformed_data, len(transformed_data)
    points = []
    for i in xrange(len(transformed_data)):
        point = transformed_data[i]
        if labels_dict is None:
            # points.append({"x":round(point[0],5),"y":round(point[1],5), "c_type": ctype[i]})
            points.append({"x":round(point[0],5),"y":round(point[1],5), "c_type": str(0)})
        else:
            for k in labels_dict:
                if i in labels_dict[k]:
                    points.append({"x":round(point[0],5),"y":round(point[1],5), "c_type": str(k)})
    # return json.dumps(points)
    return points

def perform_mds(data, similarity="euclidean", labels_dict=None):
    if similarity == 'euclidean':
        mds = MDS(n_components=2, max_iter=100, n_init=1, eps=0.01)
    else:
        if similarity == 'cosine':
            data = pairwise_distances(data, metric='cosine')
        else:
            data = pairwise_distances(data, metric='correlation')
        mds = MDS(n_components=2, max_iter=100, n_init=1, eps=0.01, dissimilarity='precomputed')

    transformed_data = mds.fit_transform(data)
    # ctype = []
    # for c in data['c_type']:
    #     ctype.append(c)
    points = []
    for i in xrange(len(transformed_data)):
        point = transformed_data[i]
        if labels_dict is None:
            # points.append({"x":round(point[0],5),"y":round(point[1],5), "c_type": ctype[i]})
            points.append({"x":round(point[0],5),"y":round(point[1],5), "c_type": str(0)})
        else:
            for k in labels_dict:
                if i in labels_dict[k]:
                    points.append({"x":round(point[0],5),"y":round(point[1],5), "c_type": str(k)})
    # return json.dumps(points)
    return points

def perform_isomap(data, labels_dict=None):
    iso = Isomap(n_components=2)
    transformed_data = iso.fit_transform(data)
    # ctype = []
    # for c in data['c_type']:
    #     ctype.append(c)
    points = []
    for i in xrange(len(transformed_data)):
        point = transformed_data[i]
        if labels_dict is None:
            # points.append({"x":round(point[0],5),"y":round(point[1],5), "c_type": ctype[i]})
            points.append({"x":round(point[0],5),"y":round(point[1],5), "c_type": str(0)})
        else:
            for k in labels_dict:
                if i in labels_dict[k]:
                    points.append({"x":round(point[0],5),"y":round(point[1],5), "c_type": str(k)})
    # return json.dumps(points)
    return points

# Run kmeans clustering iteratively with k=1...30
# Using elbow curve to decide k=3
def perform_kmeans(data, k=4, iterative_kmeans=False):
    # data = data.drop('c_type', axis=1)
    # print data.info
    if iterative_kmeans:
        w_obj = {}
        f = open(DATA_SUPPORT_PATH, "wb")
        kmeans_inertia = []
        for i in xrange(1, 21):
            kmeans = KMeans(n_clusters = i, n_jobs = 3)
            kmeans.fit(data)
            print i, kmeans.inertia_
            kmeans_inertia.append({"x": i, "y": kmeans.inertia_})
        w_obj["kmeans_inertia"] = kmeans_inertia
        print w_obj
        f.write(json.dumps(w_obj))
        f.close()
        return kmeans
    else:
        kmeans = KMeans(n_clusters = k, n_jobs = 1)
        kmeans.fit(data)
        # print "perform_kmeans", Counter(kmeans.labels_)
        return kmeans

'''def preprocess_data(path):
    return path
    # names = df.columns.values
    # print names
    # print data
    fr = open(path, 'rb')
    paths = path.split('/')
    new_path = paths[0]+'/'+paths[1]+'.processed'
    if os.path.exists(new_path):
        return new_path
    fw = open(new_path, 'wb')
    line = fr.readline()
    cols = line.split(',')
    cols[10] = "wa"
    cols[11] = "soil_type"
    cols[12] = cols[-1]
    wline = ",".join(cols[:13])
    fw.write(wline)
    line = fr.readline()
    # count = 10
    while(line):
        cols = line.split(',')
        cols[10] = "".join(cols[10:14])
        cols[11] = "".join(cols[14:54])
        digits = str(cols[10])
        for i in xrange(len(digits)):
            if digits[i] == '1':
                cols[10] = str(i+1)
        digits = str(cols[11])
        # t = 1
        for i in xrange(len(digits)):
            if digits[i] == '1':
                cols[11] = str(i+1)
                # t -= 1
        # if t != 0:
        #     print "!!!!!!!!!!!!!!!!!!!!!"
        #     break
        cols[12] = cols[-1]
        wline = ",".join(cols[:13])
        fw.write(wline)
        line = fr.readline()
        # count -= 1
        # if(count == 0):
        #     break
    fr.close()
    fw.close()
    return new_path
'''
def tokenize(text):
    # print text
    tokens = nltk.word_tokenize(text)
    stems = []
    for item in tokens:
        stems.append(PorterStemmer().stem(item))
    # print stems
    return stems

def text_processing():
    token_dict = {}
    for dirpath, dirs, files in os.walk(TEXT_FILE_PATH):
        # print dirpath, dirs, files
        for f in files:
            fname = os.path.join(dirpath, f)
            # print "f=", f
            # print "fname=", fname
            with open(fname) as doc:
                text = doc.read()
                token_dict[f] = unicode(text.lower().translate(None, string.punctuation), errors='ignore')
                # token_dict[f] = text.lower().translate(None, string.punctuation)

    # print token_dict.values()
    tfidf = TfidfVectorizer(tokenizer=tokenize, stop_words='english')
    # print tfidf.get_stop_words()
    tfs = tfidf.fit_transform(token_dict.values())

    feature_names = tfidf.get_feature_names()
    # print tfidf.inverse_transform(tfs)
    # print dict(zip(feature_names, tfidf.idf_))
    # print len(feature_names)
    # print feature_names
    # for col in tfs.nonzero()[1]:
        # print col, feature_names[col], ' - ', tfs[0, col]
        # continue

    print("Performing dimensionality reduction using LSA")
    t0 = time()
    # Vectorizer results are normalized, which makes KMeans behave as
    # spherical k-means for better results. Since LSA/SVD results are
    # not normalized, we have to redo the normalization.
    svd = TruncatedSVD(n_components=2)
    normalizer = Normalizer(copy=False)
    lsa = make_pipeline(svd, normalizer)

    X = lsa.fit_transform(tfs)

    print "done in %fs" % (time() - t0)

    explained_variance = svd.explained_variance_ratio_.sum()
    print("Explained variance of the SVD step: {}%".format(
        int(explained_variance * 100)))

    km = KMeans(n_clusters=2, init='k-means++', max_iter=100, n_init=1)
    print("Clustering sparse data with %s" % km)
    t0 = time()
    km.fit(X)
    print("done in %0.3fs" % (time() - t0))

    points = []
    for i in xrange(len(X)):
        points.append({"x": round(X[i][0],5), "y": round(X[i][1],5), "c_type": str(km.labels_[i])})

    print("Top terms per cluster:")
    original_space_centroids = svd.inverse_transform(km.cluster_centers_)
    order_centroids = original_space_centroids.argsort()[:, ::-1]

    # print order_centroids
    for i in range(len(order_centroids)):
       print "Cluster %d:" % i
       for ind in order_centroids[i, :20]:
           print ' %s' % feature_names[ind]
       print ""

    return points

def main():
    sampling_percent = 0.2
    num_clusters = 3
    only_plotting = True
    if not only_plotting:
        plot_data()
    else:
        output_file = WRITE_RESULTS_PATH
        f = open(output_file, "wb")
        output = {}
        # print "preprocessing data..."
        # processed_data_path = preprocess_data(FILE_PATH)
        # print "done..."
        print "loading CSV file %s..." % FILE_PATH
        data = pd.read_csv(FILE_PATH, index_col=False, delimiter=";")
        print "CSV loaded..."
        print data.info()
        random_sample = get_random_sample(data, sampling_percent)
        print "Total number of rows in random sample =",len(random_sample)
        print "random sampling done..."
        # adaptive_sample, bin_size = get_adaptive_sample(data, sampling_percent)
        # print "Total number of rows in adaptive sample =", len(adaptive_sample)
        # for i in bin_size:
        #     print "\tNums rows in bin %d = %d" % (i, bin_size[i])
        # print "adaptive sampling done..."
        print "performing pca for random sample..."
        pca_random_sample_pts = perform_pca(random_sample)
        print "done..."
        output["pca_random"] = pca_random_sample_pts
        # print "performing pca for adaptive sample..."
        # pca_adaptive_sample_pts = perform_pca(adaptive_sample)
        # output["pca_adaptive"] = pca_adaptive_sample_pts
        # print "done..."
        print "performing mds euclidean for random sample..."
        mds_random_sample_pts = perform_mds(random_sample, similarity="euclidean")
        output["mds_random_euclidean"] = mds_random_sample_pts
        print "done..."
        print "performing mds cosine for random sample..."
        mds_random_sample_pts = perform_mds(random_sample, similarity="cosine")
        output["mds_random_cosine"] = mds_random_sample_pts
        print "done..."
        print "performing mds correlation for random sample..."
        mds_random_sample_pts = perform_mds(random_sample, similarity="correlation")
        output["mds_random_correlation"] = mds_random_sample_pts
        print "done..."
        # print "performing mds for adaptive sample..."
        # mds_adaptive_sample_pts = perform_mds(adaptive_sample)
        # output["mds_adaptive"] = mds_adaptive_sample_pts
        # print "done..."
        for k in range(1,11):
            cluster_sample, bin_size, labels_dict = get_cluster_sample(data, sampling_percent, k)
            print "Total number of rows in cluster sample =", len(cluster_sample)
            for i in bin_size:
                print "\tNums rows in bin %d = %d" % (i, bin_size[i])
            print "cluster sampling done..."
            print "performing pca for cluster sample..."
            pca_cluster_sample_pts = perform_pca(cluster_sample, labels_dict)
            output["pca_cluster"+str(k)] = pca_cluster_sample_pts
            print "done..."
            print "performing mds euclidean for cluster sample..."
            mds_cluster_sample_pts = perform_mds(cluster_sample, similarity="euclidean", labels_dict=labels_dict)
            output["mds_cluster_euclidean"+str(k)] = mds_cluster_sample_pts
            print "done..."
            print "performing mds cosine for cluster sample..."
            mds_cluster_sample_pts = perform_mds(cluster_sample, similarity="cosine", labels_dict=labels_dict)
            output["mds_cluster_cosine"+str(k)] = mds_cluster_sample_pts
            print "done..."
            print "performing mds correlation for cluster sample..."
            mds_cluster_sample_pts = perform_mds(cluster_sample, similarity="correlation", labels_dict=labels_dict)
            output["mds_cluster_correlation"+str(k)] = mds_cluster_sample_pts
            print "done..."
            print "performing isomap for cluster sample..."
            isomap_cluster_sample_pts = perform_isomap(cluster_sample, labels_dict)
            output["isomap_cluster"+str(k)] = isomap_cluster_sample_pts
            print "done..."
        print "performing isomap for random sample..."
        isomap_random_sample_pts = perform_isomap(random_sample)
        output["isomap_random"] = isomap_random_sample_pts
        print "done..."
        print "performing isomap for adaptive sample..."
        # isomap_adaptive_sample_pts = perform_isomap(adaptive_sample)
        # output["isomap_adaptive"] = isomap_adaptive_sample_pts
        # print "done..."
        print "performing text processing..."
        text_processing_pts = text_processing()
        output["text_pts"] = text_processing_pts
        print "done..."
        f.write(json.dumps(output))
        f.close()

if __name__ == "__main__":
    main()
    # app.run(host='0.0.0.0',debug=True)
